# regex

A collection of interesting regular expressions to detect secrets and/or personal data.

## WORK IN PROGRESS

- https://github.com/gitleaks/gitleaks/blob/master/config/gitleaks.toml
- https://github.com/americanexpress/earlybird/blob/main/config/rules/filename.yaml
- https://github.com/americanexpress/earlybird/blob/main/config/rules/content.yaml
- https://github.com/americanexpress/earlybird/blob/main/config/rules/password-secret.yaml
- https://github.com/trufflesecurity/trufflehog/tree/main/pkg/detectors
- https://github.com/godaddy/tartufo/blob/main/tartufo/data/default_regexes.json
- https://github.com/dxa4481/truffleHogRegexes/blob/master/truffleHogRegexes/regexes.json
- https://github.com/h33tlit/secret-regex-list
- https://github.com/ankane/pdscan/blob/master/internal/rules.go
